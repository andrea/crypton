import Dashboard from '../components/Dashboard/Dashboard'
import Settings from '../components/Settings/Settings'

export const config = {
  globals: {
    appName: 'Crypton',
    appKey: 'cryptonApp',
    defaultBaseCurrency: 'USD',
  },
  api: {
    baseUrl: 'https://cryptocompare.com',
  },
  routes: {
    home: {
      path: '/',
      title: 'Dashboard',
      component: Dashboard,
    },
    settings: {
      path: '/settings',
      title: 'Settings',
      component: Settings,
    },
  },
  favorites: {
    defaultCoins: ['BTC', 'ETH', 'LTC', 'XRP', 'TRX'],
    maxFavorites: 10,
  },
}

export const theme = {
  colors: {
    darker: '#141914',
    dark: '#1b1f1b',
    medium: '#3E523E',
    light: '#CED9CE',
    lighter: '#F6FFF6',
    primary: '#51B251',
    secondary: '#FF551A',
  },
}

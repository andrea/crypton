/* eslint-disable security/detect-object-injection */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import cc from 'cryptocompare'
import DataViz from '../DataViz/DataViz'
import WithLoading from '../WithLoading/WithLoading'
import {
  checkLocalStorageKey,
  getLocalStorageKey,
  setLocalStorageKey,
} from '../../utils'
import { config } from '../../config'

class Dashboard extends Component {
  state = {
    baseCurrencies: ['EUR', 'USD'],
    favoriteCoinList: [],
    activeCoin: '',
    isLoading: true,
    prices: null,
    selectedBaseCurrency: config.globals.defaultBaseCurrency,
    error: null,
  }

  componentDidMount() {
    const { globals, routes } = config
    const { history } = this.props

    if (!checkLocalStorageKey(globals.appKey)) {
      history.push(routes.settings.path)
    }

    const settings = getLocalStorageKey(globals.appKey)

    this.setState(
      () => ({ ...settings }),
      () => {
        this.fetchPrices()
      }
    )
  }

  fetchPrices = async () => {
    const { baseCurrencies, favoriteCoinList } = this.state
    let prices

    try {
      prices = await cc.priceFull(favoriteCoinList, baseCurrencies)
    } catch (error) {
      this.setState(() => ({ error, isLoading: false }))
    }

    prices = Object.keys(prices).reduce(
      (a, v) => [...a, { [v]: prices[v] }],
      []
    )

    this.setState(() => ({ prices, isLoading: false }))
  }

  handleSetActiveCoin = activeCoin => {
    const settings = { activeCoin }
    setLocalStorageKey(config.globals.appKey, settings)
  }

  render() {
    // prettier-ignore
    const { activeCoin, favoriteCoins, isLoading, prices, selectedBaseCurrency } = this.state
    const DataVizWithLoading = WithLoading(DataViz)

    return (
      <DataVizWithLoading
        loading={isLoading}
        prices={prices}
        favoriteCoins={favoriteCoins}
        activeCoin={activeCoin}
        baseCurrency={selectedBaseCurrency}
        handleSetActiveCoin={this.handleSetActiveCoin}
      />
    )
  }
}

Dashboard.propTypes = {
  history: PropTypes.object,
}

export default Dashboard

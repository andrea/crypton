import styled from 'styled-components'

export default styled.p`
  color: ${props => props.theme.colors.light};
  font-size: 2.4rem;
`

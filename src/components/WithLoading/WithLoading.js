import React from 'react'
import PropTypes from 'prop-types'
import { HashLoader } from 'react-spinners'
import LoaderWrapper from './styled'
import { theme } from '../../config'

const WithLoading = Component =>
  function ComponentWithLoading({ loading, ...props }) {
    const { colors } = theme

    if (loading)
      return (
        <LoaderWrapper>
          <HashLoader size={40} color={colors.primary} loading={loading} />
        </LoaderWrapper>
      )

    return <Component {...props} />
  }
WithLoading.propTypes = {
  loading: PropTypes.bool,
}

export default WithLoading

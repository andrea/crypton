import styled from 'styled-components'

const CoinGrid = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 1.6rem;

  @media screen and (min-width: 480px) {
    grid-template-columns: repeat(2, 1fr);
  }

  @media screen and (min-width: 840px) {
    grid-template-columns: repeat(4, 1fr);
  }

  @media screen and (min-width: 1100px) {
    grid-template-columns: repeat(5, 1fr);
  }
`

const CoinTile = styled.button`
  font-size: 100%;
  font-weight: normal;
  font-family: Rubik, sans-serif;
  color: ${props => props.theme.colors.lighter};
  display: flex;
  justify-content: space-between;
  background-color: transparent;
  align-items: center;
  border: 1px solid transparent;
  border-radius: 0.4rem;
  opacity: ${props => (props.isFavorite ? 0.25 : 1)}
  outline: none;
  padding: 1rem;
  position: relative;
  transition: background-color 0.25s, border 0.25s;
  &:after {
    content: '';
    border-radius: 0.4rem;
    position: absolute;
    width: 100%;
    height: 100%;
    opacity: 0.05;
    top: 0;
    left: 0;
    z-index: -1;
    background-color: ${props => props.theme.colors.primary};
  }
  &:hover {
    cursor: ${props => !props.isFavorite && 'pointer'};
    color: ${props => props.theme.colors.lighter};
    border: 1px solid
      ${props =>
        props.favorites
          ? props.theme.colors.secondary
          : props.theme.colors.primary};
    &:after {
      opacity: ${props => !props.isFavorite && 0.1};
      background-color: ${props =>
        props.favorites
          ? props.theme.colors.secondary
          : props.theme.colors.primary};
    }
  }
  &:hover {
    border-color: ${props => props.isFavorite && 'transparent'};
  }
`

const TileHeader = styled.div`
  display: grid;
`

const TileName = styled.div`
  margin-top: 0.6rem;
  padding: 0.2rem 0.8rem 0.2rem 0.2rem;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`

const TileSymbol = styled.div`
  justify-self: start;
  font-size: 1.2rem;
  font-weight: 700;
  font-family: 'Source Code Pro', monospace;
  padding: 0.2rem 0.8rem;
  border-radius: 0.4rem;
  background-color: ${props => props.theme.colors.darker};
`

const TileContent = styled.div`
  display: grid;
  align-items: center;
  justify-self: right;
  padding-left: 0.8rem;
  border-left: 0.4rem dotted ${props => props.theme.colors.darker};
`

const TileImg = styled.img`
  border-radius: 50%;
  min-width: 4rem;
  height: 4rem;
  padding: 4px;
  background-color: ${props => props.theme.colors.darker};
`

export {
  CoinGrid,
  CoinTile,
  TileContent,
  TileHeader,
  TileImg,
  TileName,
  TileSymbol,
}

import React from 'react'
import PropTypes from 'prop-types'
// prettier-ignore
import { CoinGrid, CoinTile, TileContent, TileHeader, TileImg, TileName, TileSymbol, } from './styled'
import { config } from '../../config'

// prettier-ignore
const CoinList = ({ coinList, favorites, favoriteCoinList, handleUpdateCoinToFavorites }) => {
  if (!coinList) return null

  const { api } = config

  return (
    <CoinGrid>
      {coinList
        .map(coin => (
          <CoinTile
            key={coin.Id}
            favorites={favorites}
            isFavorite={
              favoriteCoinList && favoriteCoinList.includes(coin.Symbol)
            }
            disabled={favoriteCoinList && favoriteCoinList.includes(coin.Symbol)}
            data-coinsym={coin.Symbol}
            onClick={handleUpdateCoinToFavorites}
          >
            <TileHeader>
              <TileSymbol>{coin.Symbol}</TileSymbol>
              <TileName>{coin.CoinName}</TileName>
            </TileHeader>
            <TileContent>
              <TileImg src={`${api.baseUrl}/${coin.ImageUrl}`} alt="" />
            </TileContent>

          </CoinTile>
        ))
        .slice(0, 100)}
    </CoinGrid>
  )
}

CoinList.propTypes = {
  coinList: PropTypes.array.isRequired,
  favoriteCoinList: PropTypes.array,
  favorites: PropTypes.bool,
  handleUpdateCoinToFavorites: PropTypes.func.isRequired,
}

export default CoinList

/* eslint-disable security/detect-object-injection */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import numeral from 'numeral'
// prettier-ignore
import { StatsChange24H, StatsChangePCT24H, StatsContent, StatsGrid, StatsHeader, StatsPrice, StatsSymbol, StatsTile, } from './styled'

class CoinStats extends Component {
  render() {
    const { activeCoin, baseCurrency, handleSelectCoin, prices } = this.props

    return (
      <StatsGrid>
        {prices.map(price => {
          const sym = Object.keys(price)[0]
          const prc = price[sym][baseCurrency]
          const key = `${prc.FROMSYMBOL}${prc.TOSYMBOL}`
          return (
            <StatsTile
              key={key}
              activeCoin={activeCoin}
              coinSymbol={prc.FROMSYMBOL}
              data-coin={prc.FROMSYMBOL}
              priceChange24H={prc.CHANGE24HOUR}
              onClick={handleSelectCoin}
            >
              <StatsHeader>
                <StatsSymbol>
                  {prc.FROMSYMBOL}/{prc.TOSYMBOL}
                </StatsSymbol>
                <StatsPrice>{numeral(prc.PRICE).format('$0,0.00')}</StatsPrice>
              </StatsHeader>
              <StatsContent>
                <StatsChangePCT24H>
                  {numeral(prc.CHANGEPCT24HOUR / 100).format('0.00%')}
                </StatsChangePCT24H>
                <StatsChange24H>
                  {numeral(prc.CHANGE24HOUR).format('0,0.00')}
                </StatsChange24H>
              </StatsContent>
            </StatsTile>
          )
        })}
      </StatsGrid>
    )
  }
}

CoinStats.propTypes = {
  activeCoin: PropTypes.string,
  baseCurrency: PropTypes.string,
  handleSelectCoin: PropTypes.func,
  prices: PropTypes.array,
}

export default CoinStats

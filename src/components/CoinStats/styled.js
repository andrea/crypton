import styled from 'styled-components'

const StatsGrid = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 1.6rem;
  margin-bottom: 4rem;

  @media screen and (min-width: 480px) {
    grid-template-columns: repeat(2, 1fr);
  }

  @media screen and (min-width: 680px) {
    grid-template-columns: repeat(3, 1fr);
  }

  @media screen and (min-width: 840px) {
    grid-template-columns: repeat(4, 1fr);
  }

  @media screen and (min-width: 1100px) {
    grid-template-columns: repeat(5, 1fr);
  }
`

const StatsTile = styled.button`
  color: ${props => props.theme.colors.light};
  background-color: transparent;
  align-items: center;
  border: 1px solid
    ${props =>
      props.priceChange24H > 0
        ? props.theme.colors.primary
        : props.theme.colors.secondary};
  border-radius: 0.4rem;
  cursor: pointer;
  padding: 1rem;
  outline: none;
  box-shadow: 0 0 0 4px
    ${props =>
      props.activeCoin === props.coinSymbol
        ? props.theme.colors.medium
        : 'transparent'};
  position: relative;
  transition: background-color 0.25s, border 0.25s;
  &:after {
    content: '';
    border-radius: 0.4rem;
    position: absolute;
    width: 100%;
    height: 100%;
    opacity: 0.05;
    top: 0;
    left: 0;
    z-index: -1;
    background-color: ${props =>
      props.priceChange24H > 0
        ? props.theme.colors.primary
        : props.theme.colors.secondary};
  }
  &:hover {
    color: ${props => props.theme.colors.lighter};
    box-shadow: 0 0 0 4px ${props => props.theme.colors.medium};
    &:after {
      opacity: 0.1;
    }
  }
`

const StatsHeader = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
`

const StatsChangePCT24H = styled.div`
  font-family: Rubik, sans-serif;
  font-size: 3.2rem;
  padding-right: 0.8rem;
  white-space: nowrap;
  overflow: hidden;
`

const StatsChange24H = styled.div``

const StatsSymbol = styled.div`
  justify-self: start;
  font-size: 1.2rem;
  font-weight: 700;
  font-family: 'Source Code Pro', monospace;
  padding: 0.2rem 0;
  border-radius: 0.4rem;
`

const StatsPrice = styled.div`
  justify-self: end;
  font-size: 1.2rem;
  font-weight: 700;
  font-family: 'Source Code Pro', monospace;
  padding: 0.2rem 0.8rem;
  border-radius: 0.4rem;
  background-color: ${props => props.theme.colors.darker};
`

const StatsContent = styled.div`
  display: flex;
  width: 100%;
  margin-top: 0.6rem;
  align-items: baseline;
  justify-content: start;
`

export {
  StatsGrid,
  StatsPrice,
  StatsTile,
  StatsContent,
  StatsHeader,
  StatsChange24H,
  StatsChangePCT24H,
  StatsSymbol,
}

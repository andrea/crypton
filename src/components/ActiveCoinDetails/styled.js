import styled from 'styled-components'

const DetailsWrapper = styled.div`
  grid-column-start: 1;
  grid-column-end: 3;
  grid-auto-flow: row;
  color: ${props => props.theme.colors.light};
  border-radius: 0.4rem;
  padding: 0.8rem;
  background-color: ${props => props.theme.colors.darker};

  @media screen and (min-width: 680px) {
    grid-column-end: 4;
  }

  @media screen and (min-width: 840px) {
    grid-column-end: 2;
  }

  @media screen and (min-width: 1100px) {
    grid-column-end: 2;
  }
`

const DetailsHeader = styled.div`
  color: ${props => props.theme.colors.primary};
  display: flex;
  align-content: center;
  justify-content: space-between;
  padding-bottom: 0.4rem;
  margin-bottom: 1.2rem;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`

const DetailsSymbol = styled.p`
  font-size: 2.4rem;
  white-space: nowrap;
  overflow: hidden;
  padding: 0.4rem;
  text-overflow: ellipsis;
`

const DetailsContent = styled.div`
  font-size: 1.2rem;
  display: flex;
  align-items: center;
  line-height: 1.6;
  padding: 0.4rem;
  justify-content: space-between;
`

const DetailsSection = styled.div`
  color: ${props => props.theme.colors.medium};
  border-bottom: 2px solid ${props => props.theme.colors.dark};
  padding: 0.4rem;
  margin: 0.4rem 0;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`

const DetailName = styled.div`
  padding-right: 0.4rem;
`

const DetailImg = styled.img`
  border-radius: 50%;
  min-width: 3.2rem;
  height: 3.2rem;
  padding: 4px;
  background-color: ${props => props.theme.colors.darker};
`

const DetailDescription = styled.div``

// prettier-ignore
export { DetailImg, DetailName, DetailDescription, DetailsHeader, DetailsContent, DetailsSection, DetailsSymbol, DetailsWrapper }

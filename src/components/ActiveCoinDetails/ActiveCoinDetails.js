import React from 'react'
import PropTypes from 'prop-types'
import numeral from 'numeral'
// prettier-ignore
import { DetailDescription, DetailImg, DetailName, DetailsContent, DetailsHeader, DetailsSection, DetailsSymbol, DetailsWrapper } from './styled'
import { config } from '../../config'

const ActiveCoinDetails = ({ activeCoin, coinDetails }) => {
  if (!activeCoin.data || !activeCoin.baseCurrency) return null

  const { baseCurrency, data } = activeCoin

  // eslint-disable-next-line security/detect-object-injection
  const coinDailyStats = data[baseCurrency]

  return (
    <DetailsWrapper>
      <DetailsHeader>
        <DetailsSymbol>{coinDetails.CoinName}</DetailsSymbol>
        <DetailImg
          src={`${config.api.baseUrl}/${coinDetails.ImageUrl}`}
          alt=""
        />
      </DetailsHeader>
      <DetailsSection>
        <DetailName>Coin Stats</DetailName>
      </DetailsSection>
      <DetailsContent>
        <DetailName>Algorithm:</DetailName>
        <DetailDescription>{coinDetails.Algorithm}</DetailDescription>
      </DetailsContent>
      <DetailsContent>
        <DetailName>Fully Premined:</DetailName>
        <DetailDescription>
          {coinDetails.FullyPremined === '0' ? 'No' : 'Yes'}
        </DetailDescription>
      </DetailsContent>
      <DetailsContent>
        <DetailName>Premined Value:</DetailName>
        <DetailDescription>
          {coinDetails.PreMinedValue === '0' ? 'No' : 'Yes'}
        </DetailDescription>
      </DetailsContent>
      <DetailsContent>
        <DetailName>Total Coin Supply:</DetailName>
        <DetailDescription>
          {numeral(coinDetails.TotalCoinSupply).format('0 a')}
        </DetailDescription>
      </DetailsContent>
      <DetailsSection>
        <DetailName>Trading Stats</DetailName>
      </DetailsSection>
      <DetailsContent>
        <DetailName>Open 24h:</DetailName>
        <DetailDescription>
          {numeral(coinDailyStats.OPEN24HOUR).format('$0,0.00')}
        </DetailDescription>
      </DetailsContent>
      <DetailsContent>
        <DetailName>High 24h:</DetailName>
        <DetailDescription>
          {numeral(coinDailyStats.HIGH24HOUR).format('$0,0.00')}
        </DetailDescription>
      </DetailsContent>
      <DetailsContent>
        <DetailName>Low 24h:</DetailName>
        <DetailDescription>
          {numeral(coinDailyStats.LOW24HOUR).format('$0,0.00')}
        </DetailDescription>
      </DetailsContent>
    </DetailsWrapper>
  )
}

ActiveCoinDetails.propTypes = {
  activeCoin: PropTypes.object,
  coinDetails: PropTypes.object,
}

export default ActiveCoinDetails

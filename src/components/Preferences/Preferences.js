import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import fuzzy from 'fuzzy'
import { debounce } from 'lodash'
import CoinList from '../CoinList/CoinList'
// prettier-ignore
import { SearchCriteria, SearchForm, SearchInput, SearchSelect, SectionHeading, } from './styled'

class Preferences extends Component {
  state = {
    searchTerm: '',
    searchCriteria: 'all',
    searchResults: [],
    allCoins: [],
  }

  componentDidMount() {
    this.setState(() => ({ allCoins: this.props.coinLists.allCoins }))
  }

  componentWillUnmount() {
    this.debouncedFiltering.cancel()
  }

  handleInputChange = event => {
    event.persist()
    this.setState(
      () => ({ searchTerm: event.target.value }),
      // prettier-ignore
      () => { this.debouncedFiltering() }
    )
  }

  handleChangeCriteria = event => {
    event.persist()
    this.setState(() => ({ searchCriteria: event.target.value }))
  }

  handleCoinFiltering = () => {
    const { allCoins, searchCriteria, searchTerm } = this.state

    let searchData

    switch (searchCriteria) {
      case 'coinName':
        searchData = this.state.allCoins.map(({ CoinName }) => CoinName)
        break
      case 'symbol':
        searchData = this.state.allCoins.map(({ Symbol }) => Symbol)
        break
      default:
        searchData = this.state.allCoins
          .map(({ CoinName, Symbol }) => [CoinName, Symbol])
          .reduce((acc, val) => acc.concat(val), [])
    }

    const fuzzyResults = fuzzy.filter(searchTerm.trim(), searchData)

    const searchResults = allCoins.filter(({ CoinName, Symbol }) =>
      fuzzyResults.some(val => [CoinName, Symbol].includes(val.string))
    )

    this.setState(() => ({ searchResults }))
  }

  debouncedFiltering = debounce(this.handleCoinFiltering, 500)

  // TODO: Implement better solution like react-select
  render() {
    const { searchResults } = this.state
    const fullList =
      searchResults.length > 0 ? searchResults : this.props.coinLists.allCoins
    return (
      <Fragment>
        <SectionHeading>Favorite Coins</SectionHeading>
        <CoinList
          favorites
          coinList={this.props.coinLists.favoriteCoins}
          handleUpdateCoinToFavorites={this.props.handleUpdateCoinToFavorites}
        />
        <SectionHeading>Find Coins</SectionHeading>
        <SearchForm>
          <SearchInput onChange={this.handleInputChange} />
          <SearchCriteria>
            <SearchSelect
              value={this.state.searchCriteria}
              onChange={this.handleChangeCriteria}
            >
              <option value="all">Name and Symbol</option>
              <option value="coinName">Coin Name</option>
              <option value="symbol">Symbol</option>
            </SearchSelect>
          </SearchCriteria>
        </SearchForm>
        <SectionHeading>Available Cryptocurrencies</SectionHeading>
        <CoinList
          coinList={fullList}
          favoriteCoinList={this.props.favoriteCoinList}
          handleUpdateCoinToFavorites={this.props.handleUpdateCoinToFavorites}
        />
      </Fragment>
    )
  }
}

Preferences.propTypes = {
  coinLists: PropTypes.object,
  favoriteCoinList: PropTypes.array,
  handleCoinFiltering: PropTypes.func,
  handleUpdateCoinToFavorites: PropTypes.func,
}

export default Preferences

import styled from 'styled-components'

const SectionHeading = styled.p`
  color: ${props => props.theme.colors.medium}
  font-size: 2.4rem;
  text-align: center;
  padding: 3.2rem 0 2.4rem;
`

const SearchForm = styled.form`
  display: flex;
  flex-direction: column;
  font-size: 100%;
  font-weight: normal;
  font-family: Rubik, sans-serif;
  color: ${props => props.theme.colors.light};
  justify-content: space-between;
  align-items: center;
  border: 1px solid transparent;
  border-radius: 0.4rem;
  padding: 0.8rem;
  position: relative;
  &:after {
    content: '';
    border-radius: 0.4rem;
    position: absolute;
    width: 100%;
    height: 100%;
    opacity: 0.05;
    top: 0;
    left: 0;
    z-index: -1;
    background-color: ${props => props.theme.colors.primary};
  }

  @media screen and (min-width: 680px) {
    flex-direction: row;
  }
`

const SearchInput = styled.input`
  border: none;
  border-radius: 4px;
  color: ${props => props.theme.colors.light};
  font-size: 2.4rem;
  padding: 0.8rem;
  width: 100%;
  background-color: ${props => props.theme.colors.darker};
  transition: border 0.2s, outline 0.2s, box-shadow 0.2s;
  &:active,
  &:focus {
    outline: none;
    border-color: ${props => props.theme.colors.primary};
    box-shadow: 0 0 0 1px ${props => props.theme.colors.primary};
  }
`

const SearchCriteria = styled.div`
  display: block;
  position: relative;
  padding: 0.8rem 0 0;
  width: 100%;

  @media screen and (min-width: 680px) {
    display: inline-block;
    padding: 0 0 0 0.8rem;
    width: auto;
  }
`

const SearchSelect = styled.select`
  display: inline-block;
  font-size: 1.6rem;
  line-height: 1.8;
  font-weight: normal;
  font-family: Rubik, sans-serif;
  cursor: pointer;
  outline: 0;
  border: 0;
  width: 100%;
  border-radius: 4px;
  padding: 0.8rem 1.6rem;
  -webkit-appearance: none;
  -moz-appearance: none;
  //noinspection CssUnknownProperty
  appearance: none;
  color: ${props => props.theme.colors.light};
  background-color: ${props => props.theme.colors.darker};
  transition: border 0.2s, outline 0.2s, box-shadow 0.2s;
  &:active,
  &:focus {
    outline: none;
    border-color: ${props => props.theme.colors.primary};
    box-shadow: 0 0 0 1px ${props => props.theme.colors.primary};
  }
`

export { SearchCriteria, SearchForm, SearchInput, SearchSelect, SectionHeading }

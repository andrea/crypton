import React from 'react'
import PropTypes from 'prop-types'
import { ResponsiveLine } from '@nivo/line'
import { ChartWrapper } from './styled'
import { theme } from '../../config'

const ActiveCoinChart = ({ activeCoin }) => {
  const chartTheme = {
    axis: {
      domain: {
        line: {
          stroke: 'transparent',
          strokeWidth: 0,
        },
      },
      ticks: {
        line: {
          strokeWidth: 0,
        },
        text: {
          fill: theme.colors.light,
          fontSize: 10,
        },
      },
    },
    grid: {
      line: {
        stroke: theme.colors.dark,
        strokeWidth: 1,
        strokeDasharray: '2 2',
      },
    },
    tooltip: {
      container: {
        background: theme.colors.dark,
      },
    },
  }

  return (
    <ChartWrapper>
      <ResponsiveLine
        data={activeCoin.historicalDaily}
        margin={{ left: 48, bottom: 16, top: 8, right: 8 }}
        animate
        isInteractive
        enableStackTooltip
        enableGridX={true}
        enableGridY={true}
        motionStiffness={90}
        motionDamping={15}
        colors={[theme.colors.medium]}
        enableArea
        dotColor={theme.colors.primary}
        dotSize={7}
        lineWidth={2}
        lineColor={theme.colors.light}
        dotBorderColor={theme.colors.darker}
        dotBorderWidth={1}
        xScale={{ type: 'time', format: '%s', precision: 'day' }}
        axisLeft={{ format: '($' }}
        yScale={{ type: 'linear', stacked: false, min: 0, max: 'auto' }}
        axisBottom={{ format: '%b %d', tickSize: 0, tickValues: 5 }}
        theme={chartTheme}
      />
    </ChartWrapper>
  )
}

ActiveCoinChart.propTypes = {
  activeCoin: PropTypes.object,
}

export default ActiveCoinChart

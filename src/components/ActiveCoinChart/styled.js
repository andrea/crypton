import styled from 'styled-components'

const ChartWrapper = styled.div`
  grid-column-start: 1;
  grid-column-end: 3;
  grid-auto-flow: row;
  color: ${props => props.theme.colors.light};
  font-family: Rubik, sans-serif;
  border-radius: 0.4rem;
  height: 32rem;
  padding: 0.8rem;
  background-color: ${props => props.theme.colors.darker};

  @media screen and (min-width: 680px) {
    grid-column-end: 4;
    height: 48rem;
  }

  @media screen and (min-width: 840px) {
    grid-column-start: 2;
    grid-column-end: 5;
  }

  @media screen and (min-width: 1100px) {
    grid-column-start: 2;
    grid-column-end: 6;
  }
`

// eslint-disable-next-line import/prefer-default-export
export { ChartWrapper }

import styled from 'styled-components'

const ActiveCoin = styled.section`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 1.6rem;
  margin-top: 4rem;

  @media screen and (min-width: 480px) {
    grid-template-columns: repeat(2, 1fr);
  }

  @media screen and (min-width: 680px) {
    grid-template-columns: repeat(3, 1fr);
  }

  @media screen and (min-width: 840px) {
    grid-template-columns: repeat(4, 1fr);
  }

  @media screen and (min-width: 1100px) {
    grid-template-columns: repeat(5, 1fr);
  }
`

const SectionHeading = styled.p`
  color: ${props => props.theme.colors.medium}
  font-size: 2.4rem;
  text-align: center;
  padding: 3.2rem 0 2.4rem;
`

export { ActiveCoin, SectionHeading }

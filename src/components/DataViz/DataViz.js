/* eslint-disable security/detect-object-injection */
import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import cc from 'cryptocompare'
import CoinStats from '../CoinStats/CoinStats'
import ActiveCoinChart from '../ActiveCoinChart/ActiveCoinChart'
import ActiveCoinDetails from '../ActiveCoinDetails/ActiveCoinDetails'
import WithLoading from '../WithLoading/WithLoading'
import { ActiveCoin } from './styled'

class DataViz extends Component {
  state = {
    isLoading: true,
    activeCoin: {
      baseCurrency: '',
      name: '',
      data: null,
      historicalDaily: null,
    },
  }

  componentDidMount() {
    const { activeCoin, baseCurrency, prices } = this.props
    if (prices && prices.length > 0) {
      const name = activeCoin
      const data = prices.filter(coin => coin[name])[0][name]
      this.setState(
        () => ({ activeCoin: { name, data, baseCurrency } }),
        () => {
          this.fetchDailyHistorical(name, baseCurrency)
        }
      )
    }
  }

  // noinspection JSCheckFunctionSignatures
  componentDidUpdate(_, prevState) {
    const { activeCoin } = this.state

    if (activeCoin && prevState.activeCoin.name !== activeCoin.name) {
      return this.fetchDailyHistorical(activeCoin.name, activeCoin.baseCurrency)
    }

    return false
  }

  fetchDailyHistorical = (coin, baseCurrency) => {
    const options = {
      limit: 30,
    }

    this.setState(
      () => ({ isLoading: true }),
      async () => {
        const result = await cc.histoDay(coin, baseCurrency, options)
        const data = result.map(day => ({ x: day.time, y: day.close }))
        const historicalDaily = [{ id: coin, data }]
        this.setState(prevState => ({
          activeCoin: { ...prevState.activeCoin, historicalDaily },
          isLoading: false,
        }))
      }
    )
  }

  handleSelectCoin = event => {
    event.persist()
    const { prices, handleSetActiveCoin } = this.props
    const name = event.currentTarget.dataset.coin
    const data = prices.filter(coin => coin[name])[0][name]

    this.setState(
      prevState => ({ activeCoin: { ...prevState.activeCoin, name, data } }),
      () => {
        handleSetActiveCoin(name)
      }
    )
  }

  render() {
    const { activeCoin, isLoading } = this.state
    const { prices, baseCurrency, favoriteCoins } = this.props

    if (!prices) return null

    const ActiveCoinChartWithLoading = WithLoading(ActiveCoinChart)
    const coinDetails = favoriteCoins.find(
      coin => coin.Symbol === activeCoin.name
    )

    return (
      <Fragment>
        <CoinStats
          baseCurrency={baseCurrency}
          prices={prices}
          activeCoin={activeCoin.name}
          handleSelectCoin={this.handleSelectCoin}
        />
        <ActiveCoin>
          <ActiveCoinDetails
            activeCoin={activeCoin}
            coinDetails={coinDetails}
          />
          <ActiveCoinChartWithLoading
            loading={isLoading}
            activeCoin={activeCoin}
          />
        </ActiveCoin>
      </Fragment>
    )
  }
}

DataViz.propTypes = {
  activeCoin: PropTypes.string,
  favoriteCoins: PropTypes.array,
  prices: PropTypes.array,
  baseCurrency: PropTypes.string,
  handleSetActiveCoin: PropTypes.func,
}

export default DataViz

import React, { Component } from 'react'
import PropTypes from 'prop-types'
import cc from 'cryptocompare'
import FirstVisit from '../FirstVisit/FirstVisit'
import WithLoading from '../WithLoading/WithLoading'
import { LeadText, PreferencesToolbar, SavePreferences, Text } from './styled'
import { config } from '../../config'
import { getLocalStorageKey, setLocalStorageKey } from '../../utils'
import Preferences from '../Preferences/Preferences'

class Settings extends Component {
  state = {
    isLoading: true,
    favoriteCoinList: [...config.favorites.defaultCoins],
    favoriteCoins: [],
    allCoins: [],
    error: null,
  }

  componentDidMount() {
    const settings = getLocalStorageKey(config.globals.appKey)

    if (settings && settings.favoriteCoinList) {
      const { favoriteCoinList } = getLocalStorageKey(config.globals.appKey)
      return this.setState(
        () => ({ favoriteCoinList }),
        () => {
          this.fetchCoinList()
        }
      )
    }

    return this.setState(() => ({ favoriteCoinList: null, isLoading: false }))
  }

  // noinspection JSCheckFunctionSignatures
  componentDidUpdate(_, prevState) {
    const { allCoins, favoriteCoinList } = this.state

    if (prevState.favoriteCoinList !== favoriteCoinList && allCoins) {
      const favoriteCoins = this.setFavoriteCoins(allCoins)
      this.setState(() => ({ favoriteCoins }))
    }
  }

  fetchCoinList = async () => {
    let coinList

    try {
      coinList = await cc.coinList()
    } catch (error) {
      this.setState(() => ({ error, isLoading: false }))
    }

    // eslint-disable-next-line security/detect-object-injection
    const allCoins = Object.keys(coinList.Data).map(key => coinList.Data[key])
    const favoriteCoins = this.setFavoriteCoins(allCoins)
    this.setState(() => ({ allCoins, favoriteCoins, isLoading: false }))
  }

  setFavoriteCoins = coinList =>
    coinList.filter(coin => this.state.favoriteCoinList.includes(coin.Symbol))

  handleSavePreferences = (key, settings) => {
    setLocalStorageKey(key, settings)
    this.props.history.push(config.routes.home.path)
  }

  handleAppInit = (key, settings) => {
    setLocalStorageKey(key, settings)
    const { favoriteCoinList } = getLocalStorageKey(key)
    this.setState(
      () => ({ favoriteCoinList }),
      () => {
        this.fetchCoinList()
      }
    )
  }

  handleUpdateCoinToFavorites = event => {
    let { favoriteCoinList } = this.state
    const coinSym = event.currentTarget.dataset.coinsym

    if (favoriteCoinList.includes(coinSym) && favoriteCoinList.length > 1) {
      favoriteCoinList = favoriteCoinList.filter(coin => coin !== coinSym)
      return this.setState(() => ({ favoriteCoinList }))
    }

    if (favoriteCoinList.includes(coinSym) && favoriteCoinList.length === 1) {
      return false
    }

    if (favoriteCoinList.length < config.favorites.maxFavorites) {
      return this.setState(() => ({
        favoriteCoinList: [...favoriteCoinList, coinSym],
      }))
    }

    return false
  }

  render() {
    // prettier-ignore
    const { allCoins, favoriteCoinList, favoriteCoins, isLoading } = this.state
    const { globals } = config

    if (!favoriteCoinList) {
      return <FirstVisit handleAppInit={this.handleAppInit} />
    }

    // Settings to save to localStorage
    const settings = {
      favoriteCoinList,
      favoriteCoins,
      activeCoin: favoriteCoinList[0],
    }

    const PreferencesWithLoading = WithLoading(Preferences)

    return (
      <div>
        <PreferencesToolbar>
          <div>
            <LeadText>Pick your favorite coins</LeadText>
            <Text>
              {/* eslint-disable-next-line prettier/prettier */}
              {`You have currently ${favoriteCoins.length} coins in your portfolio. `}{favoriteCoins.length === config.favorites.maxFavorites ? 'You reached the maximum number of favorites.' : `You can have up to a maximum of ${config.favorites.maxFavorites}.`}
            </Text>
          </div>
          <SavePreferences
            onClick={() => this.handleSavePreferences(globals.appKey, settings)}
          >
            Save Settings
          </SavePreferences>
        </PreferencesToolbar>
        <PreferencesWithLoading
          loading={isLoading}
          coinLists={{ allCoins, favoriteCoins }}
          favoriteCoinList={favoriteCoinList}
          handleUpdateCoinToFavorites={this.handleUpdateCoinToFavorites}
        />
      </div>
    )
  }
}

Settings.propTypes = {
  history: PropTypes.object.isRequired,
}

export default Settings

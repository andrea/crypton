import styled from 'styled-components'

const Layout = styled.div`
  padding: 1rem 2rem;
  max-width: 120rem;
  margin: 0 auto;

  @media screen and (min-width: 480px) {
    padding: 2rem 4rem;
  }
`

export default Layout

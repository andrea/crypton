import React, { Fragment } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import CssBaseline from '../CssBaseline/CssBaseline'
import Layout from './styled'
import NavBar from '../NavBar/NavBar'
import { config } from '../../config'

const App = () => {
  const { routes } = config

  return (
    <Router>
      <Fragment>
        <CssBaseline />
        <Layout>
          <NavBar />
          <Switch>
            <Route
              path={routes.home.path}
              exact
              component={routes.home.component}
            />
            <Route
              path={routes.settings.path}
              exact
              component={routes.settings.component}
            />
          </Switch>
        </Layout>
      </Fragment>
    </Router>
  )
}

export default App

import React from 'react'
import GlobalStyle from './styled'

const CssBaseline = () => <GlobalStyle />

export default CssBaseline

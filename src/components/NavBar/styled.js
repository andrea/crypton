import styled from 'styled-components'
import { NavLink } from 'react-router-dom'
import { Gem } from '../Logo/Logo'

const AppNav = styled.div`
  display: grid;
  grid-template-columns: auto 1fr;
  margin-bottom: 4rem;
`

const AppLogo = styled(Gem).attrs({
  className: 'logo__type',
})`
  height: 5.6rem;
  padding: 0.8rem 0.8rem 0.8rem 0;
  &:hover .logo__type {
    fill: ${props => props.theme.colors.lighter};
  }
`

const MobileMenu = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;

  @media screen and (min-width: 680px) {
    display: none;
  }
`

const Menu = styled.div`
  display: none;
  justify-content: flex-end;
  align-items: center;

  @media screen and (min-width: 680px) {
    display: flex;
  }
`

const MenuButton = styled(NavLink).attrs({
  className: 'mobile-menu-button',
})`
  color: ${props => props.theme.colors.light};
  padding: 0.2rem;
  text-decoration: none;
  transition: color 0.25s, border-bottom-color 0.25s;
  &:link,
  &:visited {
    color: ${({ theme }) => theme.colors.light};
    border-bottom: 2px solid transparent;
  }
  &:hover,
  &:active {
    color: ${({ theme }) => theme.colors.light};

    @media screen and (min-width: 680px) {
      border-bottom: 2px solid ${({ theme }) => theme.colors.light};
    }
  }
  &.active {
    color: ${props => props.theme.colors.medium};

    @media screen and (min-width: 680px) {
      border-bottom: 2px solid ${({ theme }) => theme.colors.medium};
    }
  }
  &:not(:last-of-type) {
    margin-right: 1.6rem;
  }
  & .mobile-menu-button {
    font-size: 2.4rem;
  }
  &:hover .mobile-menu-button,
  &:active .mobile-menu-button {
    color: ${({ theme }) => theme.colors.medium};
  }
`

export { AppLogo, AppNav, Menu, MenuButton, MobileMenu }

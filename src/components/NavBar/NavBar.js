import React from 'react'
import { NavLink } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { AppNav, AppLogo, Menu, MenuButton, MobileMenu } from './styled'
import { config } from '../../config'

const NavBar = () => {
  const { globals, routes } = config

  return (
    <AppNav>
      <NavLink to={config.routes.home.path}>
        <AppLogo>{globals.appName}</AppLogo>
      </NavLink>
      <MobileMenu>
        <MenuButton exact to={routes.home.path}>
          <FontAwesomeIcon
            icon="tachometer-alt"
            size="2x"
            className="mobile-menu-button"
          />
        </MenuButton>
        <MenuButton exact to={routes.settings.path}>
          <FontAwesomeIcon
            icon="cog"
            size="2x"
            className="mobile-menu-button"
          />
        </MenuButton>
      </MobileMenu>
      <Menu>
        <MenuButton exact to={routes.home.path}>
          {routes.home.title}
        </MenuButton>
        <MenuButton exact to={routes.settings.path}>
          {routes.settings.title}
        </MenuButton>
      </Menu>
    </AppNav>
  )
}

export default NavBar

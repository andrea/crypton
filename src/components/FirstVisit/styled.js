import styled from 'styled-components'

const WelcomeText = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  padding: 1.2rem;
  position: relative;
  &:after {
    content: '';
    display: block;
    position: absolute;
    background-color: ${props => props.theme.colors.medium};
    border-radius: 4px;
    opacity: 0.25;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    z-index: -1;
  }

  @media screen and (min-width: 680px) {
    flex-direction: row;
  }
`

const LeadText = styled.p`
  color: ${props => props.theme.colors.light};
  font-size: 2.4rem;
  margin-bottom: 0.8rem;
`

const Text = styled.p`
  color: ${props => props.theme.colors.light};
  font-size: 1.6rem;
  line-height: 1.6;
`

const SavePreferences = styled.button`
  color: ${props => props.theme.colors.light};
  cursor: pointer;
  font-size: 1.6rem;
  font-family: Rubik, sans-serif;
  border: none;
  padding: 0.8rem 1.6rem;
  margin: 1.6rem 0 0;
  border-radius: 4px;
  background-color: ${props => props.theme.colors.dark};
  transition: color 0.25s, background-color 0.25s;
  &:hover,
  &:focus {
    color: ${props => props.theme.colors.lighter};
    background-color: ${props => props.theme.colors.medium};
  }

  @media screen and (min-width: 680px) {
    margin: 0 0 0 0.8rem;
    white-space: nowrap;
  }
`

const Anchor = styled.a`
  text-decoration: none;
  transition: color 0.25s;
  &:link,
  &:visited {
    color: ${props => props.theme.colors.light};
    border-bottom: 1px solid ${props => props.theme.colors.light};
  }
  &:hover {
    color: ${props => props.theme.colors.primary};
    border-bottom-color: ${props => props.theme.colors.primary};
  }
`

export { Anchor, LeadText, SavePreferences, Text, WelcomeText }

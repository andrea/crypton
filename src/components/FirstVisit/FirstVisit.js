import React from 'react'
import PropTypes from 'prop-types'
import { Anchor, LeadText, SavePreferences, Text, WelcomeText } from './styled'
import { config } from '../../config'

const FirstVisit = ({ handleAppInit }) => {
  const { globals, favorites } = config
  const settings = { favoriteCoinList: favorites.defaultCoins }

  return (
    <WelcomeText>
      <div>
        <LeadText>Welcome to {globals.appName}!</LeadText>
        <Text>
          {/* eslint-disable-next-line prettier/prettier */}
          Please, initialize the app, then select your favorite coins. Buy and hodl, just don’t get <Anchor href="https://www.reddit.com/r/Bitcoin/comments/3reaz5/moment_of_silence_for_all_of_those_rekt_on_margin/" target="_blank" rel="noopener noreferrer">#rekt</Anchor>.
        </Text>
      </div>
      <SavePreferences onClick={() => handleAppInit(globals.appKey, settings)}>
        Initialize App
      </SavePreferences>
    </WelcomeText>
  )
}

FirstVisit.propTypes = {
  handleAppInit: PropTypes.func,
}

export default FirstVisit

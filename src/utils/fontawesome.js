import { library } from '@fortawesome/fontawesome-svg-core'
import { faTachometerAlt, faCog } from '@fortawesome/free-solid-svg-icons'

export default library.add(faTachometerAlt, faCog)

export const checkLocalStorageKey = key =>
  !!(localStorage && localStorage.getItem(key) !== null)

export const getLocalStorageKey = key => {
  if (localStorage && localStorage.getItem(key) !== null) {
    return JSON.parse(localStorage.getItem(key))
  }
  return null
}

export const setLocalStorageKey = (key, settings = {}) => {
  const previousSettings = getLocalStorageKey(key)
  const newSettings = { ...previousSettings, ...settings }
  if (localStorage) {
    return localStorage.setItem(key, JSON.stringify(newSettings))
  }
  return false
}

// eslint-disable-next-line import/prefer-default-export
export const truncate = width => `
    width: ${width};
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  `

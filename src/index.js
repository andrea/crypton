import React from 'react'
import ReactDOM from 'react-dom'
import { ThemeProvider } from 'styled-components'
import WebFont from 'webfontloader'
import App from './components/App/App'
import './utils/fontawesome'
import registerServiceWorker from './registerServiceWorker'
import { theme } from './config'

WebFont.load({
  google: {
    families: ['Rubik:400', 'Source Code Pro:700'],
  },
})

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <App />
  </ThemeProvider>,
  document.getElementById('root')
)
registerServiceWorker()

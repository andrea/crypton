# Crypton

A simple dashboard to keep track of your favorite cryptocurrencies.

## Requirements

To run the application you will need to:

- [Install Node.js][install node] runtime environment

## Quick Start

To run the application...

- Clone the repository
- From the project's root folder, install the application by running:

```bash
npm install
```

- Start the client application by running:

```bash
npm start
```

Now head to `http://localhost:3000` and see your running app!
